
import React from 'react';
import "./App.css";

const App = () => {
  const [count, setCount] = React.useState(0);

  const handleAdd = () => {
    setCount(prevCount => {
      return prevCount + 1
    })
  }

  const handleSub = () => {
    setCount(prevCount => {
      return prevCount - 1
    })
  }
  const handleComplex = () => {
    setTimeout(() => {
      setCount(prevCount => {
        return prevCount + 1
      })
    }, 2000);

  }

  return (
    <div>
      {count}
      <button onClick={() => handleAdd()}>ADD</button>
      <button onClick={() => handleSub()}>SUB</button>
      <button onClick={() => handleComplex()}>Add slowly</button>
    </div>
  )
}

export default App
